# CustomButtons

Client to Server replication buttons for modders of Insurgency: Sandstorm SDK.

------

To use: place CustomButtons folder in `/SandstormEditor/Insurgency/Plugins/`.

After restarting the UE4 SDK, select all three blueprint classes from `/CustomButtons/Blueprints/` and advanced-copy them to your own folder in your mods plugin directory.

To package and share you'll need to modify your `/SandstormEditor/Insurgency/Config/UserEditor.ini`

Add the folder directory to your UserEditor.ini like this. Blueprints is the folder that will contain the three classes for example.

```
[BlueprintNativizationSettings]
+ExcludedFolderPaths=/YOURMOD/Blueprints/
+ExcludedFolderPaths=/YOURMOD/...
```

If you are packaging a map make sure you do not do this to the root or maps folder, it will cause issues.